package com.akinobank.app.repositories;

import com.akinobank.app.models.Session;
import com.akinobank.app.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

//public interface SessionRepository extends JpaRepository<Session, String> {
//    Optional<Session> findByRefreshToken(String token);
//
//    Optional<Session> findByIdAndUser(String id, User user);
//
//    List<Session> findAllByUser(User user);
//
//    Boolean existsByIdAndUser(String id, User user);
//}
