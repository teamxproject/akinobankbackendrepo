package com.akinobank.app.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CompteRecoveryRequest {
    private String email, operation;

}
