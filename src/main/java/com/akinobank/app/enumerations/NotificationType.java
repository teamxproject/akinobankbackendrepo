package com.akinobank.app.enumerations;

public enum NotificationType {
    WARN,
    INFO,
    SUCCESS
}
