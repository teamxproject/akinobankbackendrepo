package com.akinobank.app.services;

import com.akinobank.app.enumerations.Role;
import com.akinobank.app.models.User;
import com.akinobank.app.repositories.UserRepository;
import com.akinobank.app.utilities.JwtUtils;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@Data
@Log4j2
public class AuthService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null)
            throw new UsernameNotFoundException("Wrong creds");
        log.info("User is found : {}", user.getNom());
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), user.getAuthorities());
    }

    public User authenticate(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            log.info("User authenticated.");
            User authenticatedUser =  userRepository.findByEmail(email);
            if (!authenticatedUser.get_2FaEnabled() && authenticatedUser.getRole().equals(Role.CLIENT))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Vous devez activer la verification en deux étapes. " +
                    "Suivez le lien que vous avez reçu lorsque vous avez été inscrit.");
            return authenticatedUser;
        } catch (DisabledException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Votre compte est desactivé");
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "L'email ou mot de passe est incorrect.");
        }
    }

    public void agentAuthenticate(String email, String password, Role role) throws Exception {
        System.out.println(role);
        if (role.equals(Role.AGENT)) {
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            } catch (DisabledException e) {
                throw new Exception("USER_DISABLED", e);
            } catch (BadCredentialsException e) {
                throw new Exception("INVALID_CREDENTIALS", e);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    public User getCurrentUser() {
        if (jwtUtils.getToken() != null && jwtUtils.getUserFromToken() != null)
            return jwtUtils.getUserFromToken();
        return userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
    }

//    public User getCurrentAdmin() {
//        return userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
//    }


}
